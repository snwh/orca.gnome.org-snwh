---
layout: default
permalink: /get-involved.html
title: "Get Involved"
---

# Getting Involved

If you're looking to get involved in the Orca project, there's several ways to help:

* [Testing Applications](#testing-apps)
* [Fixing Bugs in Applications and Toolkits](#fixing-bugs)
* [Help Improve eSpeak](#espeak-improvements)
* [Other Ways to Help](#other-ways-to-help)

## Test Applications with Orca
{: #testing-apps}

It is relatively easy to help diagnose or pinpoint screen reader issues in your favorite applications. First, set up an environment in which the only variable is that app, and another where you can have multiple versions of that app co-existing simultaneously. Then use the most bleeding-edge version of that app as often as you can.

Use that application as thoroughly as you can with Orca and when something breaks, do the following:

1. Identify as precisely as possible the version/build where things broke. The closer you can get to an exact date and/or build the better.
2. Having found the first version/build that is broken (e.g. Firefox Nightly from 15 September), reproduce the problem while capturing a full `debug.out`. (See the [Debugging Page](./debugging.html) for more information).
3. Perform the very same steps, commands etc. in the last version/build that is not broken (e.g. Firefox Nightly from 14 September), also capturing a full `debug.out`.
4. Compare the two `debug.out` files. They should be very similar because the only difference should be your chosen app, and you've taken the time to find the exact version of that app where things changed. So to find out what happened: look for events that are new or missing. A sample dissection of `debug.out` information can be found here: [Part 1](http://mail.gnome.org/archives/orca-list/2010-April/msg00137.html) and [Part 2](http://mail.gnome.org/archives/orca-list/2010-April/msg00141.html)
5. Take all of the above data along with your analysis of it and file a bug against the appropriate component. That might be Orca; it might be the app you were testing.

We spend far more time doing the above than we do fixing the problem &mdash; or reporting the problem to the developers who need to fix it when the bug is not within Orca. If you can identify the precise point when something broke *and* identify what exactly changed (e.g. the new/different at-spi event), it would help the Orca team out tremendously.

Additionally, try testing your chosen application by using *only* the keyboard. If an application is not fully keyboard navigable, it will be much harder for Orca users to use. Application developers may not be aware of those shortcomings and would need to address those.

## Fixing Bugs in Applications and Toolkits
{: #fixing-bugs}

Developers interested in helping out can implement and/or fix accessibility support in applications and toolkits. Here are some bugs Orca users need fixed:

* ["Accessibility" bugs in GNOME applications and libraries](https://gitlab.gnome.org/groups/GNOME/-/issues?state=opened&label_name[]=8.%20Accessibility)
* [Bugs filed against Orca that are "blocked" by fixes needed in applications and/or toolkits](https://gitlab.gnome.org/GNOME/orca/-/issues/?sort=created_date&state=opened&search=%5Bblocked%5D)

Additionally help can be given to address accessibility issues in major projects:

* [LibreOffice accessibility bugs](https://bugs.freedesktop.org/showdependencytree.cgi?id=36549&hide_resolved=1)
* [Gecko accessibility bugs](https://bugzilla.mozilla.org/showdependencytree.cgi?id=374212&hide_resolved=1)
* [Basic WebKitGtk accessibility bugs](https://bugs.webkit.org/showdependencytree.cgi?id=25531&hide_resolved=1)
* [Other WebKitGtk accessibility bugs](https://bugs.webkit.org/showdependencytree.cgi?id=30796&hide_resolved=1)
* [The XFCE accessibility roadmap with descriptions of issues which need to be fixed in their applications](http://wiki.xfce.org/releng/4.10/roadmap/accessibility)

## Help Improve eSpeak
{: #espeak-improvements}

For developers who are skilled in speech synthesis development or refinement, contributions to eSpeak would be greatly appreciated to help make it more human-sounding. See the [documentation on adding or improving a language in eSpeak](http://espeak.sourceforge.net/add_language.html) for more information.

## Other Ways to Help
{: #other-ways-to-help}

  * Join the [GNOME Translation Project](https://wiki.gnome.org/TranslationProject) and help translate Orca to your favorite language

