---
layout: default
permalink: /source.html
title: "Source Code"
---

# Source Code

Orca and its dependencies are already included in many Linux distributions. For more information and/or to get the code, please see the following:

 * [Orca GNOME GitLab page](https://gitlab.gnome.org/GNOME/orca/)

## Orca Dependencies

Source code for accessibility libraries:
<ul class="tiles">
    <li>
        <a href="https://gitlab.gnome.org/GNOME/atk" title="Accessibility Toolkit project on the GNOME Gitlab">
        <h3>ATK</h3>
        <p>A library  that provides interface definitions for toolkits that wish to integrate with the GNOME accessibility infrastructure</p>
        </a>
    </li>
    <li>
        <a href="https://gitlab.gnome.org/GNOME/at-spi2-core" title="at-spi2-core project on the GNOME Gitlab">
        <h3>at-spi2-core</h3>
        <p>Base DBus XML interfaces for accessibility, the accessibility registry daemon, and atspi library.</p>
        </a>
    </li>
    <li>
        <a href="https://gitlab.gnome.org/GNOME/at-spi2-atk" title="at-spi2-atk project on the GNOME Gitlab">
        <h3>at-spi2-atk</h3>
        <p>An implementation of the ATK interfaces in terms of the libatspi2 API</p>
        </a>
    </li>
    <li>
        <a href="https://gitlab.gnome.org/GNOME/pyatspi2/" title="pyatspi2 project on the GNOME Gitlab">
        <h3>pyatspi2</h3>
        <p>A Python wrapper around libatspi which provides a convenient AT-side wrapper around the DBus protocol.</p>
        </a>
    </li>
</ul>

Additional dependencies for Braille and speech output:

* [Speech Dispatcher](https://devel.freebsoft.org/speechd)
* [BRLTTY](http://mielke.cc/brltty)
* [Liblouis](http://liblouis.io)
