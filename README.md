# Orca Website

Simple and beautiful static website for Orca. Live at [orca.gnome.org](https://orca.gnome.org).

## Setup & build

You can develop or test locally on [Fedora Silverblue](https://fedoraproject.org/silverblue/) using [Toolbx](https://containertoolbx.org):

1. Set up toolbox container and clone the repo
```
toolbox create orca-website --release 39
toolbox enter orca-website
mkdir src && cd src
git clone git@ssh.gitlab.gnome.org:Teams/Websites/orca.gnome.org.git
cd release-notes
```

2. Set up ruby and install rvm
```
sudo dnf install -y curl gcc-c++
sudo dnf builddep -y ruby
\curl -sSL https://get.rvm.io | bash -s stable
echo "source ~/.rvm/scripts/rvm" >> $HOME/.bash_profile
source $HOME/.bash_profile
rvm install ruby-3.1.2
bundle install
bundle exec jekyll s
```

## Help Needed

- Translations are currently non-existent. Help appreciated.
